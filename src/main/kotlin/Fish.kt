import kotlin.random.Random

class Fish(private var energy: Int,
           private var weight: Int,
           override var currentAge: Int,
           private val maxAge: Int,
           private val name: String):
    Animal(energy, weight, currentAge, maxAge, name){

    override fun move() {
        super.move()
        println("плывет")
    }

    override fun reproduce(): Fish {
        val offspring = Fish(
            energy = Random.nextInt(1, 10),
            weight = Random.nextInt(1, 5),
            currentAge = 4,
            maxAge = this.maxAge,
            name = this.name
        )
        println("${this.name} размножился, потомок: Name - ${offspring.name}, energy - ${offspring.energy}, weight - ${offspring.weight}, " +
                "current age - ${offspring.currentAge}, maxAge - ${offspring.maxAge}")
        return offspring
    }
}