import kotlin.random.Random

class NatureReserve() {

    val animals = mutableListOf<Animal>()

    init {
        for (i in 1..2){
            val bird = Bird(50, 5, 18, 20, "Ptah$i")
            animals.add(bird)
        }

        for (i in 1..2){
            val fish = Fish(40, 3, 4, 5, "Fish$i")
            animals.add(fish)
        }

        for (i in 1..2){
            val dog = Dog(100, 30, 9, 10, "Dog$i")
            animals.add(dog)
        }

        for (i in 1..Random.nextInt(1, 2)){
            val animal = Animal(70, 20, 9, 10, "Animal$i")
            animals.add(animal)
        }
    }

    fun animalsRandomAction(){
        val newAnimals = mutableListOf<Animal>()
        val oldAnimals = mutableListOf<Animal>()
        for (animal in animals) {
            when ((1..4).random()) {
                1 -> animal.move()
                2 -> animal.eat()
                3 -> animal.sleep()
                4 -> {
                    val offspring = animal.reproduce()
                    newAnimals.add(offspring)
                }
            }
            //println("$animal age - ${animal.currentAge}")
            if (animal.isTooOld) {
                println("$animal помер...")
                oldAnimals.add(animal)
            }
        }
        animals.removeAll(oldAnimals)
        animals.addAll(newAnimals)
    }

    fun printReserveInfo(){
        println("Животных в заповеднике - ${animals.size}")
    }
}