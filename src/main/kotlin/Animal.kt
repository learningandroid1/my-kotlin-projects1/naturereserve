import kotlin.random.Random

open class Animal(
    private var energy: Int,
    private var weight: Int,
    open var currentAge: Int,
    private val maxAge: Int,
    private val name: String) {

    open val isTooOld: Boolean
        get() = currentAge >= maxAge

    fun animalInfo() = println("Name - $name, energy - $energy, weight - $weight, current age - $currentAge, maxAge - $maxAge")

    fun sleep(){
        energy += 5
        currentAge += 1
        println("$name спит")
    }

    fun eat(){
        energy += 3
        weight += 1
        tryIncrementAge()
        println("$name ест")
    }

    open fun move(){
        energy -= 5
        weight -= 1
        tryIncrementAge()
        println("$name передвигается")
    }

    // возвращает ли объект?
    open fun reproduce(): Animal{
        val offspring = Animal(
            energy = Random.nextInt(1, 10),
            weight = Random.nextInt(1, 5),
            currentAge = 9,
            maxAge = this.maxAge,
            name = this.name
        )
        println("${this.name} размножился, потомок: Name - ${offspring.name}, energy - ${offspring.energy}, " +
                "weight - ${offspring.weight}, " +
                "current age - ${offspring.currentAge}, maxAge - ${offspring.maxAge}")
        return offspring
    }

    private fun tryIncrementAge() = if (Random.nextBoolean()) currentAge += 1 else 0
}