fun main() {

    val zapovednik1 = NatureReserve()
    zapovednik1.printReserveInfo()
    println(zapovednik1.animals)

    for (i in 1..7){
        zapovednik1.animalsRandomAction()
        println("----------")
        zapovednik1.printReserveInfo()
        println("----------")
        if (zapovednik1.animals.isEmpty()) {
            println("Зверюшки закончились, заповедник пуст.")
            break
        }
    }
}