fun main() {

    val bird1 = Bird(50, 5, 18, 20, "Ptah")
    bird1.sleep()
    println(bird1.currentAge)
    println(bird1.isTooOld)
    bird1.sleep()
    println(bird1.currentAge)
    println(bird1.isTooOld)
}